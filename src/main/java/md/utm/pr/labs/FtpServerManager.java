package md.utm.pr.labs;

import md.utm.pr.labs.server.Server;
import md.utm.pr.labs.server.ftp.FtpClientHandlerFactory;

public class FtpServerManager {
	public static void main(String[] args) throws Exception {
		Server server = new Server(new FtpClientHandlerFactory());
		server.start();
		System.out.println("Server started! Press any key to stop it.");
		System.in.read();
		System.out.println("Stopping the server");
		server.stop();
	}
}
