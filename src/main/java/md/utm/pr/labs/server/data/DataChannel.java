package md.utm.pr.labs.server.data;

public interface DataChannel {
	void write(String data);
	String read();
	void close();
}
