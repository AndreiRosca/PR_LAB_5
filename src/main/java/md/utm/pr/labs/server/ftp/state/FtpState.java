package md.utm.pr.labs.server.ftp.state;

import md.utm.pr.labs.server.ftp.FtpDataChannel;
import md.utm.pr.labs.server.ftp.FtpUser;

public interface FtpState {
	void setDataChannel(FtpDataChannel channel);
	
	void setCurrentDirectory(String directory);
	String getCurrentDirectory();
	void listFiles();
	void listFilesLong();
	void makeDirectory(String file);
	void makeDirectoryWithParents(String file);
	void removeFile(String file);
	void forceRemoveFile(String file);
	void copyFile(String sourceFile, String destinationFile);
	void copyFileRecursively(String sourceFile, String destinationFile);
	void putFile();
	void getFile(String fileName);
	void moveFile(String sourceFile, String destinationFile);
	void moveFileRecursively(String sourceFile, String destinationFile);
	void close();
	void signIn(FtpUser user);

}
