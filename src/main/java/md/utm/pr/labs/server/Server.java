package md.utm.pr.labs.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server implements Runnable {
	private final ExecutorService service;
	private volatile boolean stopRequested;
	private final ClientHandlerFactory factory;
	private int port = 9090;

	public Server(ClientHandlerFactory factory) {
		this.factory = factory;
		service = Executors.newFixedThreadPool(10, new ExceptionLoggingDaemonThreadFactory());
	}
	
	public Server(ClientHandlerFactory factory, int port) {
		this(factory);
		this.port = port;
	}

	public void start() {
		service.execute(this);
	}

	public void stop() {
		stopRequested = true;
		service.shutdownNow();
	}

	@Override
	public void run() {
		try {
			ServerSocket server = new ServerSocket(port);
			while (!stopRequested) {
				Socket socket = server.accept();
				logClient(socket);
				service.submit(factory.makeClient(socket));
			}
			server.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void logClient(Socket socket) {
		System.out.println("Serving client: " + socket.getLocalAddress() + ":" 
				+ socket.getLocalPort());
	}
}
