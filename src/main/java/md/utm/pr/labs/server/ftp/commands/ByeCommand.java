package md.utm.pr.labs.server.ftp.commands;

import md.utm.pr.labs.server.ftp.FtpDataChannel;

public class ByeCommand implements Command {

	@Override
	public void execute(FtpDataChannel channel) {
		channel.getState().close();
	}

	public static final CommandCreator CREATOR = new CommandCreator() {

		@Override
		public Command makeCommand(String command) {
			return new ByeCommand();
		}

		@Override
		public boolean accepts(String command) {
			return command.equalsIgnoreCase("bye");
		}
	};
}
