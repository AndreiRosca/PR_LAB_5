package md.utm.pr.labs.server.ftp.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import md.utm.pr.labs.server.ftp.FtpDataChannel;

public class MoveFileCommand implements Command {
	private final String sourceFile;
	private final String destinationFile;
	private final boolean recursive;

	public MoveFileCommand(String sourceFile, String destinationFile, boolean recursive) {
		this.sourceFile = sourceFile;
		this.destinationFile = destinationFile;
		this.recursive = recursive;
	}

	@Override
	public void execute(FtpDataChannel channel) {
		if (recursive)
			channel.getState().moveFileRecursively(sourceFile, destinationFile);
		else
			channel.getState().moveFile(sourceFile, destinationFile);
	}

	public static final CommandCreator CREATOR = new CommandCreator() {
		private final Pattern MOVE_FILE_PATTERN = Pattern.compile("mv( -r)? (.+) (.+)");

		@Override
		public Command makeCommand(String command) {
			Matcher m = MOVE_FILE_PATTERN.matcher(command);
			if (m.find()) {
				boolean recursive = m.group(1) != null;
				String sourceFile = m.group(2);
				String destinationFile = m.group(3);
				return new MoveFileCommand(sourceFile, destinationFile, recursive);
			}
			throw new RuntimeException("Invalid move command format.");
		}

		@Override
		public boolean accepts(String command) {
			return MOVE_FILE_PATTERN.matcher(command).matches();
		}
	};
}
