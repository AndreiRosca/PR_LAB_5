package md.utm.pr.labs.server;

import java.net.Socket;

public interface ClientHandlerFactory {
	Runnable makeClient(Socket s);
}
