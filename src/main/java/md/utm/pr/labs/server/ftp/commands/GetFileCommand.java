package md.utm.pr.labs.server.ftp.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import md.utm.pr.labs.server.ftp.FtpDataChannel;

public class GetFileCommand implements Command {

	private final String fileName;

	public GetFileCommand(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public void execute(FtpDataChannel channel) {
		channel.getState().getFile(fileName);
	}

	public static final CommandCreator CREATOR = new CommandCreator() {
		private final Pattern GET_FILE_COMMAND = Pattern.compile("get (.+)");
		
		@Override
		public Command makeCommand(String command) {
			Matcher matcher = GET_FILE_COMMAND.matcher(command);
			if (matcher.find()) {
				String fileName = matcher.group(1);
				return new GetFileCommand(fileName);
			}
			throw new RuntimeException("Invalid command syntax.");
		}

		@Override
		public boolean accepts(String command) {
			return GET_FILE_COMMAND.matcher(command).find();
		}
	};
}
