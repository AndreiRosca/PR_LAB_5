package md.utm.pr.labs.server.ftp.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import md.utm.pr.labs.server.ftp.FtpDataChannel;

public class ListFileCommand implements Command {
	private final boolean longListing;

	public ListFileCommand(boolean longListing) {
		this.longListing = longListing;
	}

	@Override
	public void execute(FtpDataChannel channel) {
		if (longListing)
			channel.getState().listFilesLong();
		else
			channel.getState().listFiles();
		channel.write("");
	}

	public static final CommandCreator CREATOR = new CommandCreator() {
		private final Pattern LIST_PATTERN = Pattern.compile("ls( -l)?");
		
		@Override
		public Command makeCommand(String command) {
			Matcher m = LIST_PATTERN.matcher(command);
			if (m.find()) {
				boolean longListing = m.group(1) != null;
				return new ListFileCommand(longListing);				
			}
			throw new RuntimeException("Invalid list command syntax.");
		}

		@Override
		public boolean accepts(String command) {
			return LIST_PATTERN.matcher(command).find();
		}
	};
}
