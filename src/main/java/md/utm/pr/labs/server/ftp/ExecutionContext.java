package md.utm.pr.labs.server.ftp;

public interface ExecutionContext {
	void setWorkingDirectory(String directory);
	String getWorkingDirectory();
}
