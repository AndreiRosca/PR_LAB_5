package md.utm.pr.labs.server.ftp.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import md.utm.pr.labs.server.ftp.FtpDataChannel;

public class MakeDirectoryCommand implements Command {

	private final String directoryName;
	private final boolean makeParents;

	public MakeDirectoryCommand(String directoryName, boolean makeParents) {
		this.directoryName = directoryName;
		this.makeParents = makeParents;
	}

	@Override
	public void execute(FtpDataChannel channel) {
		if (makeParents)
			channel.getState().makeDirectory(directoryName);
		else
			channel.getState().makeDirectoryWithParents(directoryName);
	}

	public static final CommandCreator CREATOR = new CommandCreator() {
		private final Pattern MAKE_DIRECTORY_COMMAND = Pattern.compile("mkdir( -p)? (.+)");

		@Override
		public Command makeCommand(String command) {
			Matcher matcher = MAKE_DIRECTORY_COMMAND.matcher(command);
			if (matcher.find()) {
				String directoryName = matcher.group(2);
				boolean makeParents = matcher.group(1) != null;
				return new MakeDirectoryCommand(directoryName, makeParents);
			}
			throw new RuntimeException("Bad command syntax.");
		}

		@Override
		public boolean accepts(String command) {
			return MAKE_DIRECTORY_COMMAND.matcher(command).find();
		}
	};
}
