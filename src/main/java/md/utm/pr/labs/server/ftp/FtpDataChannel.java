package md.utm.pr.labs.server.ftp;

import md.utm.pr.labs.server.data.DataChannel;
import md.utm.pr.labs.server.ftp.state.FtpStateContext;

public interface FtpDataChannel extends DataChannel, FtpStateContext {
	
}
