package md.utm.pr.labs.server.ftp.state;

public interface FtpStateContext {
	FtpState getState();
	void setState(FtpState state);
}
