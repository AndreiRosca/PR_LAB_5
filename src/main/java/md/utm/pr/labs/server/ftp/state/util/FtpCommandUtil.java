package md.utm.pr.labs.server.ftp.state.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import md.utm.pr.labs.server.ftp.FtpDataChannel;
import md.utm.pr.labs.server.ftp.FtpUser;
import md.utm.pr.labs.server.ftp.commands.PutFileCommand.FilenameNotFoundException;

public class FtpCommandUtil {
	private static final Pattern HEADER_PATTERN = Pattern.compile("(.+): (.+)");
	private static final Pattern FILENAME_PATTERN = Pattern.compile("filename=\"(.+)\";");
	
	public void deleteDirectory(File fileToDelete) {
		try {
			FileUtils.deleteDirectory(fileToDelete);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void copy(File source, File destination, FtpDataChannel channel) {
		if (!source.isDirectory()) {
			copyFile(source, destination);
			channel.write("200 OK. File " + destination + " created.");
		} else {
			channel.write("300, Source file is a directory. Use -r key.");
		}
	}

	public void copyRecursively(File source, File destination, FtpDataChannel channel) {
		if (!source.isDirectory()) {
			copyFile(source, destination);
		} else {
			copyDirectory(source, destination);
		}
		channel.write("200 OK. File " + destination + " created.");
	}

	private void copyDirectory(File source, File destination) {
		try {
			FileUtils.copyDirectory(source, destination);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void copyFile(File source, File destination) {
		try {
			FileUtils.copyFile(source, destination);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void listFilesLongListing(FtpDataChannel channel) {
		File currentDirectory = new File(channel.getState().getCurrentDirectory());
		String fileListing = new FileListPrinter(currentDirectory).list();
		channel.write(fileListing);
	}

	public void listFilesBriefly(FtpDataChannel channel) {
		File currentDirectory = new File(channel.getState().getCurrentDirectory());
		File[] files = currentDirectory.listFiles();
		for (File f : files) {
			if (f.isDirectory()) {
				channel.write(f.getName() + "/");
			} else {
				channel.write(f.getName());
			}
		}
	}

	public void writeBody(FtpDataChannel channel, String fileName) {
		String filePath = getAbsolutePath(channel, fileName);
		try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(filePath))) {
			byte[] buffer = new byte[4096];
			int bytesRead;
			while ((bytesRead = in.read(buffer)) != -1) {
				ByteBuffer encodedFrame = Base64.getEncoder().encode(ByteBuffer.wrap(buffer, 0, bytesRead));
				String frame = new String(encodedFrame.array());
				channel.write(frame);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void writeHeaders(FtpDataChannel channel, String fileName) {
		String filePath = getAbsolutePath(channel, fileName);
		channel.write("Content-Transfer-Encoding: base64");
		channel.write("Content-Length: " + new File(filePath).length());
		channel.write("");
	}

	public String getAbsolutePath(FtpDataChannel channel, String fileName) {
		return channel.getState().getCurrentDirectory() + File.separator + fileName;
	}

	public void moveDirectory(File source, File destination) {
		try {
			FileUtils.moveDirectory(source, destination);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void moveFile(File source, File destination) {
		try {
			FileUtils.moveFile(source, destination);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void move(File source, File destination, FtpDataChannel channel) {
		if (!source.isDirectory()) {
			moveFile(source, destination);
			channel.write("200 OK. File " + destination + " created.");
		} else {
			channel.write("300, 300, Source file is a directory. Use -r key.");
		}
	}

	public void moveRecursively(File source, File destination, FtpDataChannel channel) {
		if (!source.isDirectory())
			moveFile(source, destination);
		else
			moveDirectory(source, destination);
		channel.write("200 OK. File " + destination + " created.");
	}

	public void readBody(Map<String, String> headers, FtpDataChannel channel) {
		int bufferSize = Integer.valueOf(headers.get("Content-Length"));
		ByteArrayOutputStream out = new ByteArrayOutputStream(bufferSize);
		while (true) {
			String encodedFrame = channel.read();
			if (encodedFrame.equals(""))
				break;
			try {
				byte[] frame = Base64.getDecoder().decode(encodedFrame.getBytes());
				out.write(frame);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		String fileName = getFileName(headers);
		writeFile(out.toByteArray(), makeAbsolutePath(channel, fileName));
		channel.write(String.format("200 OK. File %s created.", fileName));
	}

	private String makeAbsolutePath(FtpDataChannel channel, String fileName) {
		return channel.getState().getCurrentDirectory() + File.separator + fileName;
	}

	private String getFileName(Map<String, String> headers) {
		String disposition = headers.get("Content-Disposition");
		Matcher m = FILENAME_PATTERN.matcher(disposition);
		if (m.find()) {
			return m.group(1);
		}
		throw new FilenameNotFoundException("Content-Disposition: " + disposition);
	}

	private void writeFile(byte[] byteArray, String fileName) {
		try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(fileName))) {
			out.write(byteArray);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Map<String, String> readHeaders(FtpDataChannel channel) {
		Map<String, String> headers = new HashMap<>();
		String header = "";
		do {
			header = channel.read();
			Matcher m = HEADER_PATTERN.matcher(header);
			if (m.find())
				headers.put(m.group(1), m.group(2));
		} while (!"".equals(header));
		return headers;
	}

	private static class FileListPrinter {
		private final StringBuilder builder = new StringBuilder();
		private final File directory;

		public FileListPrinter(File directory) {
			this.directory = directory;
		}

		public String list() {
			File[] files = directory.listFiles();
			for (File f : files) {
				printFile(f);
			}
			return builder.substring(0, builder.length() - 2).toString();
		}

		private void printFile(File f) {
			builder.append(getFileType(f));
			builder.append(getFileAttributes(f));
			builder.append(getFileLength(f));
			builder.append("\t");
			builder.append(getLastModification(f));
			builder.append("\t");
			builder.append(f.getName());
			builder.append("\r\n");
		}

		private String getLastModification(File f) {
			SimpleDateFormat format = new SimpleDateFormat("MMM d HH:mm");
			Date lastModified = new Date(f.lastModified());
			return format.format(lastModified);
		}

		private String getFileLength(File f) {
			return String.format("%12d", f.length());
		}

		private String getFileAttributes(File f) {
			String readable = f.canRead() ? "r" : "-";
			String writeable = f.canWrite() ? "w" : "-";
			String executable = f.canExecute() ? "x" : "-";
			return readable + writeable + executable;
		}

		private String getFileType(File f) {
			return f.isDirectory() ? "d" : "-";
		}
	}

	public FtpUser getUser(Map<String, String> headers) {
		String username = headers.get("Username");
		String password = headers.get("Password");
		return new FtpUser(username, base64Decode(password));
	}

	private String base64Decode(String str) {
		byte[] decoded = Base64.getDecoder().decode(str.getBytes());
		return new String(decoded);
	}
}
