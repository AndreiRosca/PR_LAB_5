package md.utm.pr.labs.server.ftp.state;

import md.utm.pr.labs.server.ftp.FtpDataChannel;
import md.utm.pr.labs.server.ftp.FtpUser;

public class UnauthenticatedFtpState extends AbstractFtpState {

	public UnauthenticatedFtpState(FtpDataChannel channel) {
		super(channel);
	}

	private void rejectOperation() {
		channel.write("301, Unauthorized.");
	}

	@Override
	public void makeDirectory(String file) {
		rejectOperation();
	}

	@Override
	public void removeFile(String file) {
		rejectOperation();
	}

	@Override
	public void forceRemoveFile(String file) {
		rejectOperation();
	}

	@Override
	public void copyFile(String sourceFile, String destinationFile) {
		rejectOperation();
	}

	@Override
	public void copyFileRecursively(String sourceFile, String destinationFile) {
		rejectOperation();
	}

	@Override
	public void putFile() {
		rejectOperation();
	}

	@Override
	public void moveFile(String sourceFile, String destinationFile) {
		rejectOperation();
	}

	@Override
	public void makeDirectoryWithParents(String file) {
		rejectOperation();
	}

	@Override
	public void moveFileRecursively(String sourceFile, String destinationFile) {
		rejectOperation();
	}

	@Override
	public void signIn(FtpUser user) {
		if (user.getUsername().equals("root") && user.getPassword().equals("admin")) {
			channel.setState(new AuthenticatedFtpState(channel, getCurrentDirectory()));
			channel.write("200, Successfully signed in.");			
		} else {
			channel.write("300, sign in unsuccessfull.");
		}
	}
}
