package md.utm.pr.labs.server.ftp.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import md.utm.pr.labs.server.ftp.FtpDataChannel;

public class CopyFileCommand implements Command {
	private final String sourceFile;
	private final String destinationFile;
	private final boolean recursiveCopy;

	public CopyFileCommand(String sourceFile, String destinationFile, boolean recursiveCopy) {
		this.sourceFile = sourceFile;
		this.destinationFile = destinationFile;
		this.recursiveCopy = recursiveCopy;
	}

	@Override
	public void execute(FtpDataChannel channel) {
		if (recursiveCopy)
			channel.getState().copyFileRecursively(sourceFile, destinationFile);
		else
			channel.getState().copyFile(sourceFile, destinationFile);
	}

	public static final CommandCreator CREATOR = new CommandCreator() {
		private final Pattern COPY_FILE_PATTERN = Pattern.compile("cp( -r)? (.+) (.+)");

		@Override
		public Command makeCommand(String command) {
			Matcher m = COPY_FILE_PATTERN.matcher(command);
			if (m.find()) {
				boolean recursiveCopy = m.group(1) != null;
				String sourceFile = m.group(2);
				String destinationFile = m.group(3);
				return new CopyFileCommand(sourceFile, destinationFile, recursiveCopy);
			}
			throw new RuntimeException("Invalid copy command format.");
		}

		@Override
		public boolean accepts(String command) {
			return COPY_FILE_PATTERN.matcher(command).matches();
		}
	};
}
