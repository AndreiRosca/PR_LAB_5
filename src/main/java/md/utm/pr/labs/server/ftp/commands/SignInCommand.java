package md.utm.pr.labs.server.ftp.commands;

import java.util.Map;

import md.utm.pr.labs.server.ftp.FtpDataChannel;
import md.utm.pr.labs.server.ftp.FtpUser;
import md.utm.pr.labs.server.ftp.state.util.FtpCommandUtil;

public class SignInCommand implements Command {
	private FtpCommandUtil utility = new FtpCommandUtil();
	
	@Override
	public void execute(FtpDataChannel channel) {
		Map<String, String> headers = utility.readHeaders(channel);
		FtpUser user = utility.getUser(headers);
		channel.getState().signIn(user);
	}

	public static final CommandCreator CREATOR = new CommandCreator() {

		@Override
		public Command makeCommand(String command) {
			return new SignInCommand();
		}

		@Override
		public boolean accepts(String command) {
			return command.equals("SIGN IN");
		}
	};
}
