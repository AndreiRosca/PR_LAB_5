package md.utm.pr.labs.server.ftp.commands;

import java.util.regex.Pattern;

import md.utm.pr.labs.server.ftp.FtpDataChannel;

public class PutFileCommand implements Command {

	@Override
	public void execute(FtpDataChannel channel) {
		channel.getState().putFile();
	}

	public static final CommandCreator CREATOR = new CommandCreator() {
		private final Pattern PUT_FILE_COMMAND = Pattern.compile("put");

		@Override
		public Command makeCommand(String command) {
			return new PutFileCommand();
		}

		@Override
		public boolean accepts(String command) {
			return PUT_FILE_COMMAND.matcher(command).find();
		}
	};

	public static class FilenameNotFoundException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public FilenameNotFoundException(String message) {
			super(message);
		}

		public FilenameNotFoundException() {
		}
	}
}
