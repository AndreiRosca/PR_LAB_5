package md.utm.pr.labs.server.ftp.commands;

import md.utm.pr.labs.server.ftp.FtpDataChannel;

public class CommandFactory {
	private final CommandCreator[] creators = {
			MakeDirectoryCommand.CREATOR, ByeCommand.CREATOR,
			GetFileCommand.CREATOR, PutFileCommand.CREATOR,
			ListFileCommand.CREATOR, RemoveFileCommand.CREATOR,
			ChangeWorkingDirectoryCommand.CREATOR,
			CopyFileCommand.CREATOR, MoveFileCommand.CREATOR,
			SignInCommand.CREATOR,
	};
	
	public Command makeCommand(String command) {
		for (CommandCreator creator : creators) {
			if (creator.accepts(command))
				return creator.makeCommand(command);
		}
		return NULL_COMMAND;
	}
	
	public static final Command NULL_COMMAND = new Command() {
		public void execute(FtpDataChannel channel) {
			System.out.println("Null command");
		}
	};
}
