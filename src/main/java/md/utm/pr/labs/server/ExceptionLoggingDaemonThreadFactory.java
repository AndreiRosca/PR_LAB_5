package md.utm.pr.labs.server;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class ExceptionLoggingDaemonThreadFactory implements ThreadFactory, UncaughtExceptionHandler {

	@Override
	public Thread newThread(Runnable r) {
		Thread thread = Executors.defaultThreadFactory().newThread(r);
		thread.setUncaughtExceptionHandler(this);
		thread.setDaemon(true);
		return thread;
	}

	@Override
	public void uncaughtException(Thread t, Throwable e) {
		e.printStackTrace();
	}
}
