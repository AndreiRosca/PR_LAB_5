package md.utm.pr.labs.server.ftp.state;

import java.io.File;
import java.util.Map;

import md.utm.pr.labs.server.ftp.FtpDataChannel;
import md.utm.pr.labs.server.ftp.FtpUser;

public class AuthenticatedFtpState extends AbstractFtpState {

	public AuthenticatedFtpState(FtpDataChannel channel, String currentDirectory) {
		super(channel);
		setCurrentDirectory(currentDirectory);
	}

	@Override
	public void makeDirectory(String fileName) {
		File file = new File(utility.getAbsolutePath(channel, fileName));
		file.mkdir();
		channel.write("Directory: " + file + " created");
	}

	@Override
	public void makeDirectoryWithParents(String fileName) {
		File file = new File(utility.getAbsolutePath(channel, fileName));
		file.mkdirs();
		channel.write("Directories: " + file + " created");
	}

	@Override
	public void removeFile(String fileName) {
		File file = new File(utility.getAbsolutePath(channel, fileName));
		if (file.isDirectory()) {
			channel.write("300, Directory not empty.");
			return;
		} else {
			file.delete();
		}
		channel.write("200 OK, File " + fileName + " deleted.");
	}

	@Override
	public void forceRemoveFile(String fileName) {
		File file = new File(utility.getAbsolutePath(channel, fileName));
		utility.deleteDirectory(file);
	}

	@Override
	public void copyFile(String sourceFile, String destinationFile) {
		File source = new File(utility.getAbsolutePath(channel, sourceFile));
		File destination = new File(utility.getAbsolutePath(channel, destinationFile));
		utility.copy(source, destination, channel);
	}

	@Override
	public void copyFileRecursively(String sourceFile, String destinationFile) {
		File source = new File(utility.getAbsolutePath(channel, sourceFile));
		File destination = new File(utility.getAbsolutePath(channel, destinationFile));
		utility.copyRecursively(source, destination, channel);
	}

	@Override
	public void putFile() {
		Map<String, String> headers = utility.readHeaders(channel);
		utility.readBody(headers, channel);
	}

	@Override
	public void moveFile(String sourceFile, String destinationFile) {
		File source = new File(utility.getAbsolutePath(channel, sourceFile));
		File destination = new File(utility.getAbsolutePath(channel, destinationFile));
		utility.move(source, destination, channel);
	}

	@Override
	public void moveFileRecursively(String sourceFile, String destinationFile) {
		File source = new File(utility.getAbsolutePath(channel, sourceFile));
		File destination = new File(utility.getAbsolutePath(channel, destinationFile));
		utility.moveRecursively(source, destination, channel);
	}

	@Override
	public void close() {
		channel.close();
	}

	@Override
	public void signIn(FtpUser user) {
		channel.write("300, Already signed in.");
	}
}
