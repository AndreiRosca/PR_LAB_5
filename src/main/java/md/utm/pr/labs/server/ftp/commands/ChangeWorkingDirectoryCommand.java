package md.utm.pr.labs.server.ftp.commands;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import md.utm.pr.labs.server.ftp.FtpDataChannel;

public class ChangeWorkingDirectoryCommand implements Command {
	private final String workingDirectory;

	public ChangeWorkingDirectoryCommand(String directory) {
		this.workingDirectory = directory;
	}

	@Override
	public void execute(FtpDataChannel channel) {
		if (new File(workingDirectory).isAbsolute()) {
			channel.getState().setCurrentDirectory(workingDirectory);
		} else {
			String dir = channel.getState().getCurrentDirectory();
			channel.getState().setCurrentDirectory(dir + File.separator + workingDirectory);
		}
		channel.write("200 OK, Current directory: " + 
				makeCanonical(channel.getState().getCurrentDirectory()));
	}

	private String makeCanonical(String dir) {
		try {
			return new File(dir).getCanonicalPath();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static final CommandCreator CREATOR = new CommandCreator() {
		private final Pattern CHANGE_DIRECTORY_PATTERN = Pattern.compile("cd (.+)");
		
		@Override
		public Command makeCommand(String command) {
			Matcher m = CHANGE_DIRECTORY_PATTERN.matcher(command);
			if (m.find()) {
				String directory = m.group(1);
				return new ChangeWorkingDirectoryCommand(directory);				
			}
			throw new RuntimeException("Invalid change directory command syntax.");
		}

		@Override
		public boolean accepts(String command) {
			return CHANGE_DIRECTORY_PATTERN.matcher(command).find();
		}
	};
}
