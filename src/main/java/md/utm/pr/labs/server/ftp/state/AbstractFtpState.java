package md.utm.pr.labs.server.ftp.state;

import md.utm.pr.labs.server.ftp.FtpDataChannel;
import md.utm.pr.labs.server.ftp.state.util.FtpCommandUtil;

public abstract class AbstractFtpState implements FtpState {

	protected FtpDataChannel channel;
	private String workingDirectory;
	protected FtpCommandUtil utility = new FtpCommandUtil();

	public AbstractFtpState(FtpDataChannel channel) {
		this.channel = channel;
	}

	@Override
	public void setDataChannel(FtpDataChannel channel) {
		this.channel = channel;
	}

	@Override
	public void setCurrentDirectory(String directory) {
		this.workingDirectory = directory;
	}

	@Override
	public String getCurrentDirectory() {
		return workingDirectory;
	}

	@Override
	public void listFiles() {
		utility.listFilesBriefly(channel);
	}

	@Override
	public void listFilesLong() {
		utility.listFilesLongListing(channel);
	}

	@Override
	public void getFile(String fileName) {
		utility.writeHeaders(channel, fileName);
		utility.writeBody(channel, fileName);
	}

	@Override
	public void close() {
		channel.close();
	}
}
