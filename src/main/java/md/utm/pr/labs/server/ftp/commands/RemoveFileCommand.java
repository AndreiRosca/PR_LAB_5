package md.utm.pr.labs.server.ftp.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import md.utm.pr.labs.server.ftp.FtpDataChannel;

public class RemoveFileCommand implements Command {

	private final String fileName;
	private final boolean forceRemove;

	public RemoveFileCommand(String fileName, boolean forceRemove) {
		this.fileName = fileName;
		this.forceRemove = forceRemove;
	}

	@Override
	public void execute(FtpDataChannel channel) {
		if (forceRemove) {
			channel.getState().forceRemoveFile(fileName);
		} else {
			channel.getState().removeFile(fileName);
		}
	}

	public static final CommandCreator CREATOR = new CommandCreator() {
		private final Pattern REMOVE_PATTERN = Pattern.compile("rm (-rf )?(.+)");
		
		@Override
		public Command makeCommand(String command) {
			Matcher m = REMOVE_PATTERN.matcher(command);
			if (m.find()) {
				boolean forceRemove = m.group(1) != null;
				String fileName = m.group(2);
				return new RemoveFileCommand(fileName, forceRemove);
			}
			throw new RuntimeException("Invalid filename format.");
		}

		@Override
		public boolean accepts(String command) {
			return REMOVE_PATTERN.matcher(command).find();
		}
	};
}
