package md.utm.pr.labs.server.ftp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import md.utm.pr.labs.server.ftp.commands.Command;
import md.utm.pr.labs.server.ftp.commands.CommandFactory;
import md.utm.pr.labs.server.ftp.state.FtpState;
import md.utm.pr.labs.server.ftp.state.UnauthenticatedFtpState;

public class FtpClientHandler implements Runnable, FtpDataChannel {

	private final Socket socket;
	private final PrintWriter out;
	private final BufferedReader in;
	private final CommandFactory factory = new CommandFactory();
	private volatile boolean connectionClosed;
	private FtpState currentState = new UnauthenticatedFtpState(this);

	public FtpClientHandler(Socket socket) {
		this.socket = socket;
		try {
			out = new PrintWriter(socket.getOutputStream());
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		write("Welcome, client!");
		while (!connectionClosed) {
			String command = read();
			Command c = factory.makeCommand(command);
			c.execute(this);
		}
		closeResources();
	}

	public void closeConnection() {
		connectionClosed = true;
	}

	private void closeResources() {
		try {
			out.close();
			in.close();
			socket.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void write(String message) {
		out.print(message + "\r\n");
		out.flush();
	}

	@Override
	public String read() {
		try {
			return in.readLine();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void close() {
		closeConnection();
	}

	@Override
	public FtpState getState() {
		return currentState;
	}

	@Override
	public void setState(FtpState state) {
		this.currentState = state;
	}
}
