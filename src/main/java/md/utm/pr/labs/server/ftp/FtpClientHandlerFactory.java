package md.utm.pr.labs.server.ftp;

import java.net.Socket;

import md.utm.pr.labs.server.ClientHandlerFactory;

public class FtpClientHandlerFactory implements ClientHandlerFactory {

	@Override
	public Runnable makeClient(Socket s) {
		return new FtpClientHandler(s);
	}
}
