package md.utm.pr.labs.server.ftp.commands;

public interface CommandCreator {
	Command makeCommand(String command);
	boolean accepts(String command);

}
