package md.utm.pr.labs.server.ftp.commands;

import md.utm.pr.labs.server.ftp.FtpDataChannel;

public interface Command {

	void execute(FtpDataChannel channel);

}
