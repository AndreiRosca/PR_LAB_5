package md.utm.pr.labs.chat.commands;

import md.utm.pr.labs.chat.ChatCommand;
import md.utm.pr.labs.chat.ChatCommandCreator;
import md.utm.pr.labs.chat.ChatDataChannel;

public class ByeCommand implements ChatCommand {
	
	@Override
	public void execute(ChatDataChannel channel) {
		channel.getState().logOut();
		channel.close();
	}

	public static final ChatCommandCreator CREATOR = new ChatCommandCreator() {

		@Override
		public boolean accepts(String command) {
			return command.equalsIgnoreCase("bye");
		}

		@Override
		public ChatCommand makeCommand(String command) {
			return new ByeCommand();
		}
	};
}
