package md.utm.pr.labs.chat;

import java.net.Socket;

import md.utm.pr.labs.server.ClientHandlerFactory;

public class ChatClientHandlerFactory implements ClientHandlerFactory {

	@Override
	public Runnable makeClient(Socket s) {
		return new ChatClientHandler(s);
	}
}
