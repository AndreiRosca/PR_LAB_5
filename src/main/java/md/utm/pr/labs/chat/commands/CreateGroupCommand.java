package md.utm.pr.labs.chat.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import md.utm.pr.labs.chat.ChatCommand;
import md.utm.pr.labs.chat.ChatCommandCreator;
import md.utm.pr.labs.chat.ChatDataChannel;

public class CreateGroupCommand implements ChatCommand {
	private final String groupName;

	public CreateGroupCommand(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public void execute(ChatDataChannel channel) {
		channel.getState().createGroup(groupName);
		channel.write("200, Group " + groupName + " created.");
	}

	public static final ChatCommandCreator CREATOR = new ChatCommandCreator() {
		private final Pattern CREATE_GROUP_PATTERN = Pattern.compile("CREATE GROUP (.+)");

		@Override
		public boolean accepts(String command) {
			return CREATE_GROUP_PATTERN.matcher(command).matches();
		}

		@Override
		public ChatCommand makeCommand(String command) {
			Matcher m = CREATE_GROUP_PATTERN.matcher(command);
			if (m.find()) {
				String groupName = m.group(1);
				return new CreateGroupCommand(groupName);
			}
			return null;
		}
	};
}
