package md.utm.pr.labs.chat.state;

import md.utm.pr.labs.chat.ChatDataChannel;
import md.utm.pr.labs.chat.ChatRegistry;
import md.utm.pr.labs.chat.GroupMessage;
import md.utm.pr.labs.chat.Message;
import md.utm.pr.labs.chat.User;
import md.utm.pr.labs.chat.status.ChatStatus;

public class AuthenticatedUserChatState implements ChatState {

	private ChatDataChannel channel;
	private ChatRegistry registry = ChatRegistry.getInstance();
	private final User user;

	public AuthenticatedUserChatState(ChatDataChannel channel, User user) {
		this.channel = channel;
		this.user = user;
	}

	@Override
	public void broadcastMessage(String message) {
		String envelope = buildMessageEnvelope(message);
		registry.broadcastFrom(user, w -> w.write(envelope));
	}

	private String buildMessageEnvelope(String message) {
		return new StringBuilder()
				.append("BROADCAST MESSAGE" + "\r\n")
				.append("Content-Length: " + message.length() + "\r\n")
				.append("Content-Transfer-Encoding: text/plain" + "\r\n")
				.append("Message-Author: " + user.getUsername() + "\r\n")
				.append("\r\n")
				.append(message)
				.append("\r\n")
				.toString();
	}

	@Override
	public void sendMessage(Message m) {
		String envelope = buildPrivateMessageEnvelope(m);
		registry.sendPrivateMessage(m.getReceiver(), envelope);
	}

	private String buildPrivateMessageEnvelope(Message m) {
		return new StringBuilder()
				.append("PRIVATE MESSAGE" + "\r\n")
				.append("Content-Length: " + m.getMessage().length() + "\r\n")
				.append("Content-Transfer-Encoding: text/plain" + "\r\n")
				.append("Message-Author: " + user.getUsername() + "\r\n")
				.append("Recipient: " + m.getReceiver().getUsername() + "\r\n")
				.append("\r\n")
				.append(m.getMessage())
				.append("\r\n")
				.toString();
	}

	@Override
	public void postGroupMessage(GroupMessage m) {
		String envelope = buildGroupMessageEnvelope(m);
		registry.postGroupMessage(GroupMessage.newBuilder()
				.setMessage(envelope)
				.setAuthor(m.getAuthor())
				.setGroupName(m.getGroupName())
				.build());
	}

	private String buildGroupMessageEnvelope(GroupMessage m) {
		return new StringBuilder()
				.append("GROUP MESSAGE" + "\r\n")
				.append("Content-Length: " + m.getMessage().length() + "\r\n")
				.append("Content-Transfer-Encoding: text/plain" + "\r\n")
				.append("Message-Author: " + m.getAuthor() + "\r\n")
				.append("Target-Group: " + m.getGroupName() + "\r\n")
				.append("\r\n")
				.append(m.getMessage())
				.append("\r\n")
				.toString();
	}

	@Override
	public void logIn(User user) {
		channel.write(ChatStatus.INVALID_STATE.getStatus());
	}

	@Override
	public void register(User user) {
		channel.write(ChatStatus.INVALID_STATE.getStatus());
	}

	@Override
	public void setDataChannel(ChatDataChannel channel) {
		this.channel = channel;
	}

	@Override
	public User getLoggedUser() {
		return user;
	}

	@Override
	public void logOut() {
		registry.logOut(user);
	}

	@Override
	public void createGroup(String groupName) {
		registry.createGroup(groupName);
		registry.addGroupMember(groupName, user);
	}

	@Override
	public void addUserToGroup(String username, String groupName) {
		registry.addGroupMember(groupName, new User(username));
	}
}
