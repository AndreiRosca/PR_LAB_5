package md.utm.pr.labs.chat.repository.impl;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import md.utm.pr.labs.chat.User;
import md.utm.pr.labs.chat.repository.UserRepository;

public class InMemoryUserRepository implements UserRepository {
	
	private static final Set<User> users = new CopyOnWriteArraySet<>();

	@Override
	public boolean register(User u) {
		return users.add(u);
	}

	@Override
	public boolean isUserRegistered(User u) {
		return users.contains(u);
	}

	@Override
	public boolean authenticate(User user) {
		User foundUser = users.stream().filter(u -> u.equals(user)).findFirst().get();
		return foundUser.getPassword().equals(user.getPassword());
	}
}
