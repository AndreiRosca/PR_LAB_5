package md.utm.pr.labs.chat.state;

import md.utm.pr.labs.chat.ChatDataChannel;
import md.utm.pr.labs.chat.GroupMessage;
import md.utm.pr.labs.chat.Message;
import md.utm.pr.labs.chat.User;

public interface ChatState {
	void setDataChannel(ChatDataChannel channel);

	void broadcastMessage(String message);
	void sendMessage(Message m);
	void postGroupMessage(GroupMessage m);
	void createGroup(String groupName);
	void addUserToGroup(String username, String groupName);
	void logIn(User user);
	void register(User user);
	User getLoggedUser();
	void logOut();

}
