package md.utm.pr.labs.chat;

public interface ChatCommandCreator {
	boolean accepts(String command);
	ChatCommand makeCommand(String command);

}
