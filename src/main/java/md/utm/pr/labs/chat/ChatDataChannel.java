package md.utm.pr.labs.chat;

import md.utm.pr.labs.chat.state.ChatStateContext;
import md.utm.pr.labs.server.data.DataChannel;

public interface ChatDataChannel extends DataChannel, ChatStateContext {

}
