package md.utm.pr.labs.chat.commands;

import java.util.Map;

import md.utm.pr.labs.chat.ChatCommand;
import md.utm.pr.labs.chat.ChatCommandCreator;
import md.utm.pr.labs.chat.ChatDataChannel;

public class BroadcastMessageCommand implements ChatCommand {
	private CommandUtil utility = new CommandUtil();

	@Override
	public void execute(ChatDataChannel channel) {
		Map<String, String> headers = utility.readHeaders(channel);
		if (utility.messageIsBase64Encoded(headers)) {
			String message = utility.base64Decode(utility.readMessageBody(channel));
			channel.getState().broadcastMessage(message);
		} else {
			channel.write("305, Invalid Transfer encoding.");
		}
	}

	public static final ChatCommandCreator CREATOR = new ChatCommandCreator() {

		@Override
		public boolean accepts(String command) {
			return command.equals("BROADCAST");
		}

		@Override
		public ChatCommand makeCommand(String command) {
			return new BroadcastMessageCommand();
		}
	};
}
