package md.utm.pr.labs.chat.commands;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import md.utm.pr.labs.chat.ChatDataChannel;
import md.utm.pr.labs.chat.User;

public class CommandUtil {
	private static final Pattern HEADER_PATTERN = Pattern.compile("(.+): (.+)");

	public Map<String, String> readHeaders(ChatDataChannel channel) {
		Map<String, String> headers = new HashMap<>();
		String header = "";
		do {
			header = channel.read();
			Matcher m = HEADER_PATTERN.matcher(header);
			if (m.find())
				headers.put(m.group(1), m.group(2));
		} while (!header.equals(""));
		return headers;
	}

	public User getUser(Map<String, String> headers) {
		String username = headers.get("USERNAME");
		String base64Password = headers.get("PASSWORD");
		return new User(username, base64Decode(base64Password));
	}

	public String base64Decode(String base64) {
		byte[] decoded = Base64.getDecoder().decode(base64.getBytes());
		return new String(decoded);
	}

	public String readMessageBody(ChatDataChannel channel) {
		StringBuilder message = new StringBuilder();
		String line = "";
		while (!(line = channel.read()).equals("")) {
			message.append(line);
		}
		return message.toString();
	}
	
	public boolean messageIsBase64Encoded(Map<String, String> headers) {
		String encoding = headers.get("Content-Transfer-Encoding");
		return "base64".equals(encoding);
	}
}
