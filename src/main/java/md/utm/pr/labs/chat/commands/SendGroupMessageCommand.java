package md.utm.pr.labs.chat.commands;

import java.util.Map;

import md.utm.pr.labs.chat.ChatCommand;
import md.utm.pr.labs.chat.ChatCommandCreator;
import md.utm.pr.labs.chat.ChatDataChannel;
import md.utm.pr.labs.chat.GroupMessage;

public class SendGroupMessageCommand implements ChatCommand {
	private CommandUtil utility = new CommandUtil();
	
	@Override
	public void execute(ChatDataChannel channel) {
		Map<String, String> headers = utility.readHeaders(channel);
		if (utility.messageIsBase64Encoded(headers)) {
			String msg = utility.base64Decode(utility.readMessageBody(channel));
			GroupMessage message = GroupMessage.newBuilder()
					.setMessage(msg)
					.setGroupName(headers.get("Target-Group"))
					.setAuthor(channel.getState().getLoggedUser().getUsername())
					.build();
			channel.getState().postGroupMessage(message);
		} else {
			channel.write("305, Invalid transport encoding.");
		}
	}

	public static final ChatCommandCreator CREATOR = new ChatCommandCreator() {

		@Override
		public boolean accepts(String command) {
			return command.equals("GROUP MESSAGE");
		}

		@Override
		public ChatCommand makeCommand(String command) {
			return new SendGroupMessageCommand();
		}
	};
}
