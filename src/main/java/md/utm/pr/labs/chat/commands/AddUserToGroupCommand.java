package md.utm.pr.labs.chat.commands;

import java.util.Map;

import md.utm.pr.labs.chat.ChatCommand;
import md.utm.pr.labs.chat.ChatCommandCreator;
import md.utm.pr.labs.chat.ChatDataChannel;

public class AddUserToGroupCommand implements ChatCommand {
	private CommandUtil utility = new CommandUtil();

	@Override
	public void execute(ChatDataChannel channel) {
		Map<String, String> headers = utility.readHeaders(channel);
		String groupName = headers.get("Target-Group");
		String username = headers.get("Username");
		channel.getState().addUserToGroup(username, groupName);
		channel.write("200, User " + username + " added to group " + groupName + ".");
	}

	public static final ChatCommandCreator CREATOR = new ChatCommandCreator() {

		@Override
		public boolean accepts(String command) {
			return command.equals("ADD USER TO GROUP");
		}

		@Override
		public ChatCommand makeCommand(String command) {
			return new AddUserToGroupCommand();
		}
	};
}
