package md.utm.pr.labs.chat;

public class GroupMessage {
	private String groupName;
	private String message;
	private String author;

	public String getGroupName() {
		return groupName;
	}

	public String getMessage() {
		return message;
	}

	public String getAuthor() {
		return author;
	}
	
	public static GroupMessageBuilder newBuilder() {
		return new GroupMessageBuilder();
	}
	
	public static class GroupMessageBuilder {
		private GroupMessage groupMessage = new GroupMessage();
		
		public GroupMessageBuilder setGroupName(String groupName) {
			groupMessage.groupName = groupName;
			return this;
		}
		
		public GroupMessageBuilder setMessage(String message) {
			groupMessage.message = message;
			return this;
		}
		
		public GroupMessageBuilder setAuthor(String author) {
			groupMessage.author = author;
			return this;
		}
		
		public GroupMessage build() {
			return groupMessage;
		}
	}
}
