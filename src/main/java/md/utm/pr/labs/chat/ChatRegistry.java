package md.utm.pr.labs.chat;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.Consumer;

import md.utm.pr.labs.chat.repository.UserRepository;
import md.utm.pr.labs.chat.repository.impl.InMemoryUserRepository;

public class ChatRegistry {
	private static final ChatRegistry instance = new ChatRegistry();

	private Map<User, ChatWriter> onlineUsers = new ConcurrentHashMap<>();
	private UserRepository repository = new InMemoryUserRepository();
	private Map<String, Set<User>> groups = new ConcurrentHashMap<>();

	private ChatRegistry() {
	}

	public static ChatRegistry getInstance() {
		return instance;
	}

	public boolean isUserOnline(User u) {
		return onlineUsers.keySet().contains(u);
	}

	public boolean signInUser(User user, ChatWriter writer) {
		if (repository.authenticate(user)) {
			onlineUsers.put(user, writer);
			return true;
		}
		return false;
	}

	public void broadcastFrom(User author, Consumer<ChatWriter> consumer) {
		for (Map.Entry<User, ChatWriter> entry : onlineUsers.entrySet()) {
			if (!entry.getKey().equals(author))
				consumer.accept(entry.getValue());
		}
	}

	public void postGroupMessage(GroupMessage message) {
		Set<User> targetUsers = groups.get(message.getGroupName());
		for (Map.Entry<User, ChatWriter> entry : onlineUsers.entrySet()) {
			if (targetUsers.contains(entry.getKey()))
				entry.getValue().write(message.getMessage());
		}
	}

	public void sendPrivateMessage(User receiver, String message) {
		ChatWriter writer = onlineUsers.get(receiver);
		writer.write(message);
	}

	public void logOut(User user) {
		onlineUsers.remove(user);
		for (Map.Entry<String, Set<User>> entry : groups.entrySet()) {
			if (entry.getValue().contains(user))
				entry.getValue().remove(user);
		}
	}

	public void createGroup(String groupName) {
		groups.putIfAbsent(groupName, new CopyOnWriteArraySet<>());
	}

	public void addGroupMember(String groupName, User user) {
		Set<User> members = groups.get(groupName);
		if (members != null)
			members.add(user);
		else
			;
	}
}
