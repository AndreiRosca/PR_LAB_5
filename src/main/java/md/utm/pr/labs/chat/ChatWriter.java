package md.utm.pr.labs.chat;

public interface ChatWriter {
	void write(String message);
}
