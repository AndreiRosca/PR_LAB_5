package md.utm.pr.labs.chat;

public interface ChatCommand {
	void execute(ChatDataChannel channel);
}
