package md.utm.pr.labs.chat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import md.utm.pr.labs.chat.state.ChatState;
import md.utm.pr.labs.chat.state.UnauthenticatedUserChatState;

public class ChatClientHandler implements Runnable, ChatDataChannel, ChatWriter {

	private final Socket socket;
	private final PrintWriter out;
	private final BufferedReader reader;
	private volatile boolean closeRequested;
	private final ChatCommandFactory factory = new ChatCommandFactory();
	private ChatState state = new UnauthenticatedUserChatState(this);

	public ChatClientHandler(Socket socket) {
		this.socket = socket;
		try {
			out = new PrintWriter(socket.getOutputStream());
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		write("Welcome, " + socket.getLocalAddress() + ":" + socket.getLocalPort() + "!");
		while (!closeRequested) {
			String command = read();
			ChatCommand c = factory.makeCommand(command);
			c.execute(this);
		}
		closeResources();
	}

	@Override
	public void write(String message) {
		out.print(message + "\r\n");
		out.flush();
	}

	@Override
	public String read() {
		try {
			return reader.readLine();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void close() {
		closeRequested = true;
	}

	private void closeResources() {
		try {
			reader.close();
			out.close();
			socket.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public ChatState getState() {
		return state;
	}

	@Override
	public void setState(ChatState state) {
		this.state = state;
		state.setDataChannel(this);
	}
}
