package md.utm.pr.labs.chat.status;

public enum ChatStatus {
	LOGIN_SUCCESSFUL(201, "Login successfull"),
	USER_SUCCESSFULLY_REGISTERED(202, "User successfully registered"),
	
	UNAUTHORIZED(300, "Unauthorized"),
	USERNAME_ALREADY_TAKEN(301, "Username already taken"),
	LOGIN_UNSUCCESSFUL(302, "Login unsuccessful"),
	INVALID_STATE(303, "Invalid state");

	ChatStatus(int statusCode, String statusMessage) {
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
	}

	private int statusCode;
	private String statusMessage;

	public int getStatusCode() {
		return statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}
	
	public String toString() {
		return getStatus();
	}

	public String getStatus() {
		return String.format("%d, %s.", statusCode, statusMessage);
	}
}
