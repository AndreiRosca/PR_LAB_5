package md.utm.pr.labs.chat.commands;

import java.util.Map;

import md.utm.pr.labs.chat.ChatCommand;
import md.utm.pr.labs.chat.ChatCommandCreator;
import md.utm.pr.labs.chat.ChatDataChannel;
import md.utm.pr.labs.chat.User;

public class SignInCommand implements ChatCommand {
	private CommandUtil utility = new CommandUtil();

	@Override
	public void execute(ChatDataChannel channel) {
		Map<String, String> headers = utility.readHeaders(channel);
		User user = utility.getUser(headers);
		channel.getState().logIn(user);
	}

	public static final ChatCommandCreator CREATOR = new ChatCommandCreator() {

		@Override
		public boolean accepts(String command) {
			return command.equals("SIGN IN");
		}

		@Override
		public ChatCommand makeCommand(String command) {
			return new SignInCommand();
		}
	};
}
