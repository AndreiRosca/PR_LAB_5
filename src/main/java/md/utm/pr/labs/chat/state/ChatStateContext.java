package md.utm.pr.labs.chat.state;

public interface ChatStateContext {
	ChatState getState();
	void setState(ChatState state);
}
