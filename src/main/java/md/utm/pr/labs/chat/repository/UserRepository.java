package md.utm.pr.labs.chat.repository;

import md.utm.pr.labs.chat.User;

public interface UserRepository {
	boolean register(User u);
	boolean isUserRegistered(User u);
	boolean authenticate(User u);
}
