package md.utm.pr.labs.chat;

public class Message {
	private User sender;
	private User receiver;
	private String message;

	public Message() {
	}

	public User getSender() {
		return sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public String getMessage() {
		return message;
	}
	
	public static MessageBuilder newBuilder() {
		return new MessageBuilder();
	}
	
	public static class MessageBuilder {
		private Message message = new Message();
		
		public MessageBuilder setSender(User sender) {
			message.sender = sender;
			return this;
		}
		
		public MessageBuilder setReceiver(User receiver) {
			message.receiver = receiver;
			return this;
		}
		
		public MessageBuilder setMessage(String msg) {
			message.message = msg;
			return this;
		}
		
		public Message build() {
			return message;
		}
	}
}
