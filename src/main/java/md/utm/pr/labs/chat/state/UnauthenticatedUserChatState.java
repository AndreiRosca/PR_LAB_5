package md.utm.pr.labs.chat.state;

import md.utm.pr.labs.chat.ChatDataChannel;
import md.utm.pr.labs.chat.ChatRegistry;
import md.utm.pr.labs.chat.GroupMessage;
import md.utm.pr.labs.chat.Message;
import md.utm.pr.labs.chat.User;
import md.utm.pr.labs.chat.repository.UserRepository;
import md.utm.pr.labs.chat.repository.impl.InMemoryUserRepository;
import md.utm.pr.labs.chat.status.ChatStatus;

public class UnauthenticatedUserChatState implements ChatState {

	private ChatDataChannel channel;
	private ChatRegistry registry = ChatRegistry.getInstance();
	private UserRepository repository = new InMemoryUserRepository();

	public UnauthenticatedUserChatState(ChatDataChannel channel) {
		this.channel = channel;
	}

	private void rejectOperation() {
		channel.write(ChatStatus.UNAUTHORIZED.getStatus());
	}

	@Override
	public void broadcastMessage(String message) {
		rejectOperation();
	}

	@Override
	public void sendMessage(Message m) {
		rejectOperation();
	}

	@Override
	public void postGroupMessage(GroupMessage m) {
		rejectOperation();
	}

	@Override
	public void logIn(User user) {
		if (registry.signInUser(user, (message) -> channel.write(message))) {
			channel.setState(new AuthenticatedUserChatState(channel, user));
			channel.write(ChatStatus.LOGIN_SUCCESSFUL.getStatus());
		} else {
			channel.write(ChatStatus.LOGIN_UNSUCCESSFUL.getStatus());
		}
	}

	@Override
	public void register(User user) {
		if (repository.register(user)) {
			channel.write(ChatStatus.USER_SUCCESSFULLY_REGISTERED.getStatus());
		} else {
			channel.write(ChatStatus.USERNAME_ALREADY_TAKEN.getStatus());
		}
	}

	@Override
	public void setDataChannel(ChatDataChannel channel) {
		this.channel = channel;
	}

	@Override
	public User getLoggedUser() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void logOut() {
	}

	@Override
	public void createGroup(String groupName) {
		rejectOperation();
	}

	@Override
	public void addUserToGroup(String username, String groupName) {
		rejectOperation();
	}
}
