package md.utm.pr.labs.chat.commands;

import java.util.Map;

import md.utm.pr.labs.chat.ChatCommand;
import md.utm.pr.labs.chat.ChatCommandCreator;
import md.utm.pr.labs.chat.ChatDataChannel;
import md.utm.pr.labs.chat.Message;
import md.utm.pr.labs.chat.User;

public class SendPrivateMessageCommand implements ChatCommand {
	private CommandUtil utility = new CommandUtil();

	@Override
	public void execute(ChatDataChannel channel) {
		Map<String, String> headers = utility.readHeaders(channel);
		String body = utility.readMessageBody(channel);
		if (utility.messageIsBase64Encoded(headers)) {
			String decodedMessage = utility.base64Decode(body);
			channel.getState().sendMessage(Message.newBuilder()
					.setSender(channel.getState().getLoggedUser())
					.setReceiver(getMessageReceiver(headers))
					.setMessage(decodedMessage)
					.build());
		} else {
			channel.write("304, Invalid transfer encoding.");
		}
	}

	private User getMessageReceiver(Map<String, String> headers) {
		return new User(headers.get("Message-Receiver"));
	}

	public static final ChatCommandCreator CREATOR = new ChatCommandCreator() {

		@Override
		public boolean accepts(String command) {
			return command.equals("PRIVATE MESSAGE");
		}

		@Override
		public ChatCommand makeCommand(String command) {
			return new SendPrivateMessageCommand();
		}
	};
}
