package md.utm.pr.labs.chat.commands;

import java.util.Map;
import java.util.regex.Pattern;

import md.utm.pr.labs.chat.ChatCommand;
import md.utm.pr.labs.chat.ChatCommandCreator;
import md.utm.pr.labs.chat.ChatDataChannel;
import md.utm.pr.labs.chat.User;

public class RegisterCommand implements ChatCommand {
	private CommandUtil utility = new CommandUtil();

	@Override
	public void execute(ChatDataChannel channel) {
		Map<String, String> headers = utility.readHeaders(channel);
		User u = utility.getUser(headers);
		channel.getState().register(u);
	}

	public static final ChatCommandCreator CREATOR = new ChatCommandCreator() {
		private final Pattern REGISTER_PATTERN = Pattern.compile("REGISTER");

		@Override
		public boolean accepts(String command) {
			return REGISTER_PATTERN.matcher(command).matches();
		}

		@Override
		public ChatCommand makeCommand(String command) {
			return new RegisterCommand();
		}
	};
}
