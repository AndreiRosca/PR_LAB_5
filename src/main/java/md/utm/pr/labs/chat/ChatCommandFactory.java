package md.utm.pr.labs.chat;

import md.utm.pr.labs.chat.commands.AddUserToGroupCommand;
import md.utm.pr.labs.chat.commands.BroadcastMessageCommand;
import md.utm.pr.labs.chat.commands.ByeCommand;
import md.utm.pr.labs.chat.commands.CreateGroupCommand;
import md.utm.pr.labs.chat.commands.RegisterCommand;
import md.utm.pr.labs.chat.commands.SendGroupMessageCommand;
import md.utm.pr.labs.chat.commands.SendPrivateMessageCommand;
import md.utm.pr.labs.chat.commands.SignInCommand;

public class ChatCommandFactory {
	private static final ChatCommandCreator[] commands = {
			RegisterCommand.CREATOR, SignInCommand.CREATOR, 
			ByeCommand.CREATOR, BroadcastMessageCommand.CREATOR,
			SendPrivateMessageCommand.CREATOR, CreateGroupCommand.CREATOR,
			AddUserToGroupCommand.CREATOR, SendGroupMessageCommand.CREATOR,
	};

	public ChatCommand makeCommand(String command) {
		for (ChatCommandCreator c : commands)
			if (c.accepts(command))
				return c.makeCommand(command);
		return NULL_COMMAND;
	}
	
	public static final ChatCommand NULL_COMMAND = new ChatCommand() {
		public void execute(ChatDataChannel channel) {
			System.out.println("Null command fallback");
		}
	};
}
