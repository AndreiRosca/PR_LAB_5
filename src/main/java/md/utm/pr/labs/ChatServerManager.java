package md.utm.pr.labs;

import java.io.IOException;

import md.utm.pr.labs.chat.ChatClientHandlerFactory;
import md.utm.pr.labs.server.Server;

public class ChatServerManager {
	public static void main(String[] args) throws IOException {
		Server server = new Server(new ChatClientHandlerFactory(), 9999);
		server.start();
		System.out.println("Server started! Press any key to stop it.");
		System.in.read();
		System.out.println("Stopping the server");
		server.stop();
	}
}
